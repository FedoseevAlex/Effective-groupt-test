<?php
class circle
{
    public $radius;
    public $area;
    public function return_area()
    {
        $this->area=(pi()*$this->radius*$this->radius);
    }
}
class rectangle
{
    public $a;
    public $b;
    public $area;
    public function return_area()
    {
        $this->area=($this->a*$this->b);
    }
}
class triangle
{
    public $a;
    public $b;
    public $c;
    public $area;
    public function return_area()
    {
        $p=($this->a+$this->b+$this->c)/2;
        $this->area=(sqrt($p*($p-$this->a)*($p-$this->b)*($p-$this->c)));
    }
}
$dop=array();
$j = file_get_contents('figures.json' );
$data = json_decode($j);
for ($i=0;$i<sizeof($data);$i++)
{
    if ($data[$i]->{'type'}=="circle")
    {
        $dop[sizeof($dop)]=new circle();
        $dop[sizeof($dop)-1]->radius=$data[$i]->{'radius'};
    }
    if ($data[$i]->{'type'}=="rectangle")
    {
        $dop[sizeof($dop)]=new rectangle();
        $dop[sizeof($dop)-1]->a=$data[$i]->{'a'};
        $dop[sizeof($dop)-1]->b=$data[$i]->{'b'};
    }
    if ($data[$i]->{'type'}=="triangle")
    {
        $dop[sizeof($dop)]=new triangle();
        $dop[sizeof($dop)-1]->a=$data[$i]->{'a'};
        $dop[sizeof($dop)-1]->b=$data[$i]->{'b'};
        $dop[sizeof($dop)-1]->c=$data[$i]->{'c'};
    }
    $dop[sizeof($dop)-1]->return_area();
}
for ($i=0;$i<sizeof($dop);$i++)
{
    for ($j=0;$j<sizeof($dop)-1;$j++)
    {
        if ($dop[$j]->area<$dop[$j+1]->area)
        {
            $swap=$dop[$j]->area;
            $dop[$j]->area=$dop[$j+1]->area;
            $dop[$j+1]->area=$swap;
        }
    }
}
for ($i=0;$i<sizeof($dop);$i++){
    echo get_class($dop[$i]).": ".$dop[$i]->area."<br>";
}
?>
